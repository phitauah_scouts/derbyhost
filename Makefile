CPU=`dmesg | grep "CPU:" | head -n 1 | sed -e 's/^.*(\\(.*\\)).*$$/\\1/g'`
PIVER=`dmesg | grep "Machine model:" | sed -e"s/.*Machine model: //g"`
DERBYNET_ENV_FILE=$(HOME)/.derbynet.env
DERBYNET_TIMER_ENV_FILE=$(DERBYNET_ENV_FILE).timer
DERBYNET_IMGTAG_ENV_FILE=$(DERBYNET_ENV_FILE).imgtag

all: prep run
prep: init webserver_image $(DERBYNET_TIMER_ENV_FILE)
reconfig: clean_config config

clean_config:
	rm -f $(DERBYNET_TIMER_ENV_FILE) $(DERBYNET_IMGTAG_ENV_FILE)

config: $(DERBYNET_ENV_FILE)

webserver_image:
	CPU=$(CPU); \
	if [ "$$CPU" = "ARMv7" ]; then \
		echo "I need to build a custom docker image for derbynet to run on this CPU."; \
		./docker/derbynet_build/compile_for_pi3.sh; \
	else \
		echo "I can use the standard docker image for derbynet on this CPU."; \
	fi

init:
	./init.sh

$(DERBYNET_TIMER_ENV_FILE):
	if [ -f "$DERBYNET_ENV_FILE" ]; then \
		export TIMER_DEV_PATH=`grep "TIMER_DEV_PATH" "$(DERBYNET_ENV_FILE)" | sed -e"s/^.*TIMER_DEV_PATH\s*=\s*//g"`; \
	fi; \
	if [ -z "$$TIMER_DEV_PATH" ]; then \
		export TIMER_DEV_PATH=`sh ./docker/find_timer.sh`; \
	fi; \
	echo "export TIMER_DEV_PATH=$$TIMER_DEV_PATH" > $(DERBYNET_TIMER_ENV_FILE)

$(DERBYNET_IMGTAG_ENV_FILE):
	CPU=$(CPU); \
	if [ "$$CPU" = "ARMv7" ]; then \
		export DERBYNET_IMGTAG="local:derbynet_webserver_raspi"; \
	else \
		export DERBYNET_IMGTAG="jeffpiazza/derbynet_server"; \
	fi; \
	echo "export DERBYNET_IMGTAG=$$DERBYNET_IMGTAG" > $(DERBYNET_IMGTAG_ENV_FILE)

run:
	. $(DERBYNET_ENV_FILE) && cd docker && docker-compose build && docker-compose up -d

$(DERBYNET_ENV_FILE): $(DERBYNET_IMGTAG_ENV_FILE) $(DERBYNET_TIMER_ENV_FILE)
	(cat "$(DERBYNET_IMGTAG_ENV_FILE)" && cat "$(DERBYNET_TIMER_ENV_FILE)") > "$(DERBYNET_ENV_FILE)"
