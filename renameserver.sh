NEWHOSTNAME=$1
if [ -z "$NEWHOSTNAME" ]; then
	NEWHOSTNAME="derbyhost"
fi
hostnamectl set-hostname $NEWHOSTNAME
cp /etc/hosts /etc/hosts.prev
(
echo "127.0.0.1	localhost
127.0.0.1	$NEWHOSTNAME"
grep -v "127.0.0.1" /etc/hosts.prev
) >/etc/hosts

if [ -f /etc/cloud/cloud.cfg ]; then
	sed -i -e"/preserve_hostname/s/:\s*false/: true/" /etc/cloud/cloud.cfg
fi
