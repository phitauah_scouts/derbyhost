# Configuring for your timer

The environment was created using a USB-to-Serial adapter and a FastTrack timer
with 3 lanes.  If your timer or usb connector differ, then you will need to
update the following files:

## docker-compose.yml

```yaml
services:
  timer:
    devices:
      - "/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0:/dev/ttyUSB0"
```

The devices line maps the USB controller (or timer if USB) from the Raspberry
PI to the timer docker container.  In this case...

```/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0```

... to ...

```/dev/ttyUSB0```

To help you identify the path on the pi...

1. From a command prompt, run the find_timer.sh script on the pi.

1. It will ask you to make sure the usb-to-serial adapter and/or timer are NOT
  plugged in.

1. Once you confirm, press 'enter' to scan for devices.

1. It will ask you to plug in the usb adapter and/or timer.

1. Once you confirm, press 'enter' to scan for devices again.

1. The script will then report the new devices.  This should be the path to
  include in the devices line before the colon.

## timer/boot/derbynet.config

Update the TIMER_ARGS for the timer type and number of lanes.  Refer to
[The Timer Documentation on derbynet.org](https://derbynet.org/builds/docs/Timer%2520Operation.pdf)
for details on the different options.
