#!/bin/sh
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`

if [ -n "$SUDO_USER" ]; then
  OSUSER=$SUDO_USER
else
  OSUSER=`id -run`
fi

echo "Setting up to run as: $OSUSER"

# Install updates and base packages
echo "Installing updates and necessary apt packages."
apt-get update
apt-get -y upgrade
apt-get install -y ca-certificates curl gnupg lsb-release
apt-get install -y zsh v4l-utils
apt-get install -y linux-modules-extra-raspi
echo "... done."
echo "Adding keyrings for docker and derbynet..."
DIST=$(lsb_release -is | tr 'A-Z' 'a-z')
# add signing keys for apt
rm -f /usr/share/keyrings/docker-archive-keyring.gpg /usr/share/keyrings/derbynet-archive-keyring.gpg
curl -fsSL https://download.docker.com/linux/$DIST/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
curl -fsSL https:/jeffpiazza.org/derbynet/debian/jeffpiazza_derbynet.gpg | gpg --dearmor -o /usr/share/keyrings/derbynet-archive-keyring.gpg
# add custom sources to apt
mkdir -p /etc/apt/sources.list.d/
cp -f $SCRIPTPATH/apt/sources/* /etc/apt/sources.list.d/
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian  \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# update apt list with new sources
echo "... done."
echo "Updating list of apt packaages including new sources including derbynet and docker..."
apt-get update
# Install docker
echo "... done."
echo "Installing and configuring docker..."
apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose

usermod -a -G docker $OSUSER

echo "... done."
echo "Installing and configuring network manager..."
# configure network for local network
if [ "$DIST" = "ubuntu" ]; then
  snap install network-manager
  nmcli d
  snap connect network-manager:nmcli
fi
echo "... checking for nmcli..."
if [ -n "$(which nmcli)" ]; then
  nmcli r wifi on
  if [ -n "$(nmcli c show | grep '^derbynet\s')" ]; then
    nmcli c delete derbynet
  fi
  nmcli c add type wifi con-name derbynet ifname wlan0 ssid derbynet
  nmcli c modify derbynet wifi-sec.key-mgmt wpa-psk wifi-sec.psk doyourbest
fi

echo "... done."
echo "Setting up zsh for user..."

# Configure shell as zsh
cp /etc/zsh/newuser.zshrc.recommended /home/$OSUSER/.zshrc
ZSH=`which zsh`
usermod --shell $ZSH $OSUSER
echo "... done."

#chmod -R 777 /var/lib/docker/volumes/derbynet_server_var_www/
