#!/bin/sh
SCRIPTDIR=$(dirname $(realpath $0))
ORIGPWD=$PWD
set -e
if [ ! -e "/tmp/derbynet" ]; then
    echo "Cloning repo..."
    git clone https://github.com/jeffpiazza/derbynet /tmp/derbynet
elif [ -d "/tmp/derbynet/.git" ]; then
    echo "Repo exists, updating an resetting it."
    cd /tmp/derbynet 
    git clean -f -x -d
    git fetch origin
    git checkout origin/master
    git reset --hard
    cd -
fi
echo "... done."
echo "Building local:derbynet_build_prep_raspi image..."
docker build --tag local:derbynet_build_prep_raspi -f $SCRIPTDIR/Dockerfile.prep /tmp/derbynet
echo "... done."
if [ -n "$( docker ps -a | grep derbynet_build_prep_raspi)" ]; then
    docker rm derbynet_build_prep_raspi
fi
echo "Running local:derbynet_build_prep_raspi image..."
docker run --volume /tmp/derbynet:/src --name derbynet_build_prep_raspi local:derbynet_build_prep_raspi
echo "... done."
echo "Creating local:derbynet_build_raspi image..."
docker container commit derbynet_build_prep_raspi  local:derbynet_build_raspi
docker rm derbynet_build_prep_raspi 
echo "... done."
echo "Building local:derbynet_webserver_raspi image..."
docker build --tag local:derbynet_webserver_raspi -f /tmp/derbynet/installer/docker/Dockerfile /tmp/derbynet
echo "... done."
cd $ORIGPWD
