#!/bin/bash
read -p "Make sure the timer is not plugged in and press return..." var
find /dev | sort > /tmp/pre.txt
read -p "Plug in the timer and press return..." var
find /dev | sort > /tmp/post.txt
comm -13 /tmp/pre.txt /tmp/post.txt | grep "/by\-id/"
