# Derby host repository (https://gitlab.com/phitauah_scouts/derbyhost/)

## This project relies on the derbynet software

For use of the software itself refer to <https://derbynet.org>

Its purpose is to help automate setting up a local enviroment using one
or more Raspberry PIs.  It was orignally created before Raspberry Pis\
were widely used and they were not supported by the standard distribution\
of the derbynet software.

## Recommended set up

1. One Raspberry PI (preferably PI 4 or newer) to be the main server.
   It will be running Docker with the following images.

   * Derbynet
   * The derbynet timer software.

1. An HDMI compatible projector.

1. A Wi-Fi router to create a local network for the derbynet environment.
   You can connect to an existing Wi-Fi environment just substitute wireless
   network information when setting up the Raspberry PIs.

1. A laptop for connecting to the derbynet server.  If not available,
   you will need a monitor, mouse, and keyboard connected to the main Raspberry
   PI.

### Sample set up

![physical network](physical_nw.png)

### Orchestration from a laptop

![logical network](logical_network.png)

## Pre-requisites

1. You will need a compter for setting up the Raspberry PI.  Download and
   install the Raspberry Pi Imager software from <https://www.raspberrypi.com/software/>.

## Set up the main derbyhost

1. Using The Raspberry Pi Imager software, Initialize an SD Card with the
   recommended raspberry Pi OS.

   * You should select a version with a UI.
   * The imaging software, allows you to define settings, such as host name,
      Wi-Fi connection information as a part of creating the image.  It is
      easier if you let the imager initialize its settings.
   * While you can specify any username and password, using the following
      settimgs will allow the documention to refer to the actual values.  If
      you choose your own values you will need to substitute your values when
      refered to later.
      * Username: derbyuser
        Password: doyourbest

1. Put the SD card into the Raspberry Pi and power it up.  It will boot up,
   apply some settings and then reboot.

1. When finishes updates & reboots, you should be logged into the desktop ui.

1. Open a command prompt and login as derbyuser.

1. Clone the derbyhost repo (<https://gitlab.com/phitauah_scouts/derbyhost.git>)
   onto the PI.

   ```sh
      cd ~/
      git clone https://gitlab.com/phitauah_scouts/derbyhost.git
   ```

1. Run the init.sh script from the root directory of the clone using sudo or
   as root.

   ```sh
   cd ~/derbyhost/
   sudo make prep
   ```

1. In order ensure that docker is fully set up, restart the raspberry pi

   ```sh
   sudo reboot now
   ```

1. Once the PI boots back up, open a command prompt and login as derbyuser.

1. Switch into the git clone created in step 1.  Prepare, configure, and
   run the derbynet software.

   * If you are running on a Pi that is compatible with the prebuilt derbynet
     software, it will be used as published.  Otherwise, the script will try
     to build it from source.
   * You will be asked to plug in the timer so the timer software can locate
     it.

   ```sh
   cd ~/derbyhost
   make run
   ```

1. Once the command finishes, make sure all the docker containers started
   and are running.  You are confirming that there are 2 containers running:
   * 1 for the web server.
   * 1 for the timer.

   ```sh
      $ docker ps -a --format "table {{.Image}}\t{{.Status}}\t{{.Names}}"
      IMAGE                      STATUS          NAMES
      derbynet_webserver_raspi   Up 31 minutes   docker-webserver-1
      docker-timer               Up 31 minutes   docker-timer-1
   ```

1. View the logs for the container that is not running (Ex docker-timer-1)

   ```sh
      $ docker logs docker_timer_1
      ERROR: for timer  Cannot start service timer: error gathering device information while adding custom device "/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0": no such file or directory
   ```

1. If you see an error like this, then you will need to customize your
   timer settings as defined in timer.md.

1. Once both containers are running and stay running, then you can open
   a browser to <https://derbyhost.local> and proceed to configure your
   derbynet environment.

## Important Considerations

### File persistence

This solution uses a docker volume called `docker_derbynet-cfg` to store
files created by derbynet.  It is mapped to `/var/lib/derbynet` inside the
container.  If you want any files, like photos, to be persisted after the
docker container is destroyed, you either need to make sure files get stored
under ```/var/lib/derbynet``` or that you map another volume into the docker
container in docker-compose.yml
